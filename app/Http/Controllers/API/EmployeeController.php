<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Http\Resources\EmployeeCollection;
use App\Http\Resources\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\Response;
use mysql_xdevapi\Collection;

class EmployeeController extends Controller
{
    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employees = $this->employee->paginate(20);
//        $employeeCollection = new EmployeeCollection($employees);
        $employeeCollection = EmployeeResource::collection($employees)->response()->getData(true);
        return response()->json([
            "data" => $employeeCollection,
            "message" => "Success!!"
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeRequest $request)
    {
        $dataCreate = $request->all();
        $employee = $this->employee->create($dataCreate);
        $employeeResource = new EmployeeResource($employee);

        return \response()->json([
            "data" => $employeeResource,
            "message" => "post new data success!"
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $employee = $this->employee->findOrFail($id);
        return response()->json([
            "data" => $employee,
            "message" => "Find Success!!"
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmployeeRequest $request, string $id)
    {
        $employee = $this->employee->findOrFail($id);
        $dataUpdate = $request->all();
        $employee->update($dataUpdate);
        $employeeResource = new EmployeeResource($employee);

        return \response()->json([
            "data" => $employeeResource,
            "message" => "Update Success"
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $employee = $this->employee->findOrFail($id);
        $employee->delete();
        $employeeResource = new EmployeeResource($employee);

        return \response()->json([
            "data" => $employeeResource,
            "message" => "Delete Success!"
        ], Response::HTTP_OK);
    }
}
