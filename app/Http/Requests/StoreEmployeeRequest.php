<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|min:10",
            "address" => "required",
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = new Response([
            "errors" => $validator->errors()
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator,  $response));
    }

    public function messages()
    {
        return [
            "name.required" => "The 'name' field is required. Please enter the 'name' field",
            "address.required" => "The 'address' field is required. Please enter the 'address' field",
            "name.min" => 'Name must at least 10 characters'
        ];
    }
}
