<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required_without_all:address,description",
            "address" => "required_without_all:name,description",
            "description" => "required_without_all:address,name"
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = new Response([
            "errors" => $validator->errors()
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator,  $response));
    }

    public function messages()
    {
        return [
            "name" => "The name field is required when none of address||description",
            "address" => "The address field is required when none of name||description",
            "description" => "The description field is required when none of name||address"
        ];
    }
}
