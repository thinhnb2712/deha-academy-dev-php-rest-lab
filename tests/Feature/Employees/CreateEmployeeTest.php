<?php

namespace Tests\Feature\Employees;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateEmployeeTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function user_can_create_employee_if_data_is_valid()
    {
        $dataCreate = [
            "name" => $this->faker->name,
            "address" => $this->faker->address,
            "description" => $this->faker->text
        ];

        $response = $this->json("POST", route("employees.store"), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json)=>
                $json->where("name", $dataCreate["name"])
                    ->where("address", $dataCreate["address"])
                ->etc()
            )->etc()

        );

        $this->assertDatabaseHas("employees", [
            "name" => $dataCreate["name"],
            "address" => $dataCreate["address"],
            "description" => $dataCreate["description"]
        ]);
    }

    /** @test */
    public function user_can_not_create_employee_if_name_is_null()
    {
        $dataCreate = [
            "name" => "",
            "address" => $this->faker->address,
            "description" => $this->faker->text
        ];

        $response = $this->json("POST", route("employees.store"), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("errors", fn(AssertableJson $json)=>
                $json->has("name")
            )->etc()
        );
    }

    /** @test */
    public function user_can_not_create_employee_if_address_is_null()
    {
        $dataCreate = [
            "name" => $this->faker->name,
            "address" => "",
            "description" => $this->faker->text
        ];

        $response = $this->json("POST", route("employees.store"), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
        $json->has("errors", fn(AssertableJson $json)=>
            $json->has("address"))
        );
    }

    /** @test */
    public function user_can_not_create_employee_if_name_and_address_is_null()
    {
        $dataCreate = [
            "name" => "",
            "address" => "",
            "description" => $this->faker->text
        ];

        $response = $this->json("POST", route("employees.store"), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("errors", fn(AssertableJson $json)=>
                $json->has("address")
                    ->has("name")
            )
        );
    }
}
