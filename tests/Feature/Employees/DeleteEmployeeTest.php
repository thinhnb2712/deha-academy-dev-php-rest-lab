<?php

namespace Tests\Feature\Employees;

use App\Models\Employee;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteEmployeeTest extends TestCase
{
    /** @test */
    public function user_can_delete_if_employee_exits()
    {
        $employee = Employee::factory()->create();
        $employeeCountBeforeDelete = Employee::count();
        $response =$this->json("DELETE", route("employees.destroy", $employee->id));

        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where("name", $employee->name)
                    ->where("address", $employee->address)
                    ->etc()
            )->etc()
        );
        $employeeCountAfterDelete = Employee::count();

        $this->assertEquals($employeeCountBeforeDelete-1, $employeeCountAfterDelete);
    }

    /** @test */
    public function user_can_not_delete_if_employee_not_exists()
    {
        $employeeId = -1;
        $response = $this->json("DELETE", route("employees.destroy", $employeeId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
