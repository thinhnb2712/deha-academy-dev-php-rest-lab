<?php

namespace Tests\Feature\Employees;

use App\Models\Employee;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetEmployeeTest extends TestCase
{
    /** @test */
    public function user_can_get_employee_if_data_exists()
    {
        $employee = Employee::factory()->create();

        $response = $this->getJson(route("employees.show", $employee->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("data", fn(AssertableJson $json) =>
                $json->where("name", $employee->name)
                    ->where("address", $employee->address)
                    ->where("description", $employee->description)
                    ->etc()
            )
            ->has("message")
        );
    }

    /** @test */
    public function user_can_not_get_post_if_data_not_exists()
    {
        $employeeId = -1;

        $response = $this->getJson(route("employees.show", $employeeId));

        $response->assertStatus((Response::HTTP_NOT_FOUND));
    }
}
