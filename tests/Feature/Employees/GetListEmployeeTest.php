<?php

namespace Tests\Feature\Employees;

use App\Models\Employee;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListEmployeeTest extends TestCase
{
    /** @test */
    public function user_can_get_list_employee() {
        $employeeCount = Employee::count();

        $response = $this->getJson(route('employees.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("data", fn(AssertableJson $json) =>
                $json->has("data")
                    ->has("meta", fn(AssertableJson $json) =>
                        $json->where("total",$employeeCount)
                            ->etc()
                    )->etc()
            )->etc()
            ->has("message")
        );
    }
}
