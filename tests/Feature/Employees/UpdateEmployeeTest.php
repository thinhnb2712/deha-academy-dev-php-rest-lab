<?php

namespace Tests\Feature\Employees;

use App\Models\Employee;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateEmployeeTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function user_can_update_employee_if_employee_exists_an_data_is_valid()
    {
        $employee = Employee::factory()->create();
        $dataUpdate = [
            "name" => $this->faker->name,
            "address" => $this->faker->address,
            "description" => $this->faker->text
        ];

        $response = $this->json("PUT", route("employees.update", $employee->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("data", fn(AssertableJson $json) =>
                $json->where("name", $dataUpdate["name"])
                    ->where("description", $dataUpdate["description"])
                    ->where("address", $dataUpdate["address"])
                ->etc()
            )->etc()
        );

        $this->assertDatabaseHas("employees", [
            "name" => $dataUpdate["name"],
            "address" => $dataUpdate["address"],
            "description" => $dataUpdate["description"]
        ]);
    }

    /** @test */
    public function user_can_not_update_employee_if_employee_exists_an_data_is_not_valid()
    {
        $employee = Employee::factory()->create();
        $dataUpdate = [
            "name" => "",
            "address" => "",
            "description" => ""
        ];

        $response = $this->json("PUT", route("employees.update", $employee->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has("errors")
        );
    }

    /** @test */
    public function user_can_not_update_employee_if_employee_not_exists_an_data_is_valid()
    {
        $employeeId = -1;
        $dataUpdate = [
            "name" => $this->faker->name,
            "address" => $this->faker->address,
            "description" => $this->faker->text
        ];

        $response = $this->json("PUT", route("employees.update", $employeeId), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
